## Instructions

1.  Clone repo.

2.  Configure .env file and setup database 

3.  Run migrations `artisan migrate`

4.  Seed database `artisan db:seed`  (this will add default articles to the system)

5.  Serve App `artisan serve`

Browse to http://127.0.0.1:8000/ 
