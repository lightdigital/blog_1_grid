<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;


class Blog extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blogposts';

    protected $fillable = ['title', 'body', 'image_path', 'user_id'];

    /**
     * Get the owner of the post
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
     public function user()
     {
       return $this->belongsTo(User::class);
     }

    /** Get the ratings per blog */
    public function ratings()
    {
        return $this->hasMany('App\Models\Rating');
    }     
     
}