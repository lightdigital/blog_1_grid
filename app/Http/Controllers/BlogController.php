<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Blog;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the home landing.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blogs = Blog::orderByRaw('updated_at - created_at DESC')->limit(3)->get();

        return view('home', ['blogs' => $blogs]); 
    }

    /**
     * Show the blog listing page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function blog_listing()
    {
        $blogs = Blog::orderByRaw('updated_at - created_at DESC')->get();

        return view('blog_listing', ['blogs' => $blogs]);
    }    

    /**
     * Show the blog post based on {{id}} 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function blog_post(Request $request, $id)
    {
        $blog_post = Blog::where('id', $id)->get()[0];   

        $latest_posts = Blog::orderByRaw('updated_at - created_at DESC')->get(); 

        return view('blog_post', ['blog_post' => $blog_post, 'latest_posts' => $latest_posts, 'request' => $request]);
    }    


    /**
     * Create blog post 
     *
     * @return response for Blog creation view
     */
    public function blog_post_create(Request $request)
    {
        $blog_post = false;  

        if (count($_POST) > 0) { 

            /*
            $request->validate([
                'title' => 'required|unique:posts|max:255',
                'body' => 'required',
                'updated_at' => 'nullable|date',
            ]);*/

            $blog_post = Blog::create([
                'title' => $request->input('title'),
                'body' => $request->input('body'),
                'user_id' => $request->user()->id,
                'image_path' => ' ',
                'created_at' => date("Y-m-d H:i:s"),  
                'updated_at' => date("Y-m-d H:i:s")
            ]);

            return redirect('/blog/'.$blog_post->id);
        }

        return view('blog_post_create', ['blog_post' => $blog_post, 'request' => $request]);
    }     


    /**
     * Edit the blog post based on {{id}} 
     *
     * @return response for edit view or redirect to relevant blog
     */
    public function blog_post_edit(Request $request, $id)
    {
        $blog_post = Blog::where('id', $id)
                            ->where('user_id', $request->user()->id)
                            ->get()[0];   

        if (count($_POST) > 0) { 

            /*
            $request->validate([
                'title' => 'required|unique:posts|max:255',
                'body' => 'required',
                'updated_at' => 'nullable|date',
            ]);*/    

            $blog_post->title = $request->input('title');
            $blog_post->body = $request->input('body');
            $blog_post->updated_at = date("Y-m-d H:i:s");
            $blog_post->save();

            return redirect('/blog/'.$blog_post->id);
        }

        return view('blog_post_edit', ['blog_post' => $blog_post, 'request' => $request]);
    }        


    /**
     * Remove the specified blog post.
     *
     * @param  int  $id
     * @return Response for listing view after deleted
     */
    public function blog_post_delete(Request $request, $id)
    { 
        // delete
        $blog_post = Blog::where('id', $id)
                           ->where('user_id', $request->user()->id)
                           ->get()[0];  
        $blog_post->delete();

        // redirect 
        return redirect('/blog/');
    }


    /**
     *  Adjust blog rating for specific blog.
     *
     */
    public function blog_post_rating(Request $request, $id, $rating){

        $rating = Rating::firstOrNew([
            'user_id' => $request->user()->id,
            'blog_id' => $id
        ]);
        $rating->name = $rating;
        $rating->save();        

        return response(null, 200);
    }


}
