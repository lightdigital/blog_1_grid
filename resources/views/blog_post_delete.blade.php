@extends('layouts.app')

@section('content')
<div class="container"> 

  <div class="row">
    <div class="col-md-8">

      Delete Blog Post

    {{ Form::model($blog_post, array('route' => 'blog_post_edit', $blog_post->id)) }}    

        <!-- title -->
        {{ Form::label('title', 'Title') }}
        {{ Form::text($blog_post['title']) }}

        <!-- Body -->
        {{ Form::label('body', 'Body') }}
        {{ Form::email( $blog_post['body']) }}      

        {{ Form::submit('Save Post!') }}

    {{ Form::close() }}
 
    </div>  
  
  </div>
</div>
@endsection
