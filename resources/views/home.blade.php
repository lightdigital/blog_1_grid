@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <!-- Main jumbotron for a primary marketing message or call to action -->
              <div class="jumbotron">
                <div class="container">
                  <h1 class="display-3">BLOG DEMO by Troy Changfoot</h1>
                  <p>This is a simple blog demonstration using Bootstrap v4.6.0 with Laravel 8.27.0 on PHP 8.0.2.    </p>
                  <p><a class="btn btn-primary btn-lg" href="/blog" role="button">List Articles &raquo;</a></p>
                </div>
              </div>

              <div class="container">
                <!-- Example row of columns -->
                <div class="row">
                    @foreach ($blogs as $post)                  
                      <div class="col-md-4">
                        <h2>{{ $post['title'] }}</h2>
                        <p><small>by {{ $post['created_at']->format('l jS \\of F Y h:i:s A') }}</small></p>
                        <p>{{ $post['body'] }}</p>                   
                        <p>
                            <a class="btn btn-secondary" href="/blog/{{ $post['id'] }}" role="button">View Post &raquo;</a>
                        </p>
                      </div> 
                    @endforeach  
                </div>

                <hr>

              </div> <!-- /container -->                
 
            </div>
        </div>
    </div>
</div>
@endsection
