@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-12">
                        <h1>Blog Articles</h1>
                        <hr>
                    </div>
                </div>

                <div class="row"> 
                    @foreach ($blogs as $post)                  
                      <div class="col-md-12">
                        <h2>{{ $post['title'] }}</h2>
                        <p><small>by {{ $post['created_at']->format('l jS \\of F Y h:i:s A') }}</small></p>
                        <!--- <p>{{ $post['body'] }}</p>                                    --->
                        <p>
                            <a class="btn btn-secondary" href="/blog/{{ $post['id'] }}" role="button">View Post &raquo;</a>
                        </p>
                      </div> 
                    @endforeach  
                </div>

                <hr>

            </div>               

            </div>
        </div>
    </div><!-- /container -->  
</div>
@endsection
