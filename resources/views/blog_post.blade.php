@extends('layouts.app')

@section('content')
<div class="container"> 

  <div class="row">
    <div class="col-md-8">
      <h1 class="post-title fw-500">{{ $blog_post['title'] }}</h1> 
      <small>On {{ $blog_post['created_at']->format('l jS \\of F Y h:i:s A') }} by Troy</small>
      
      <div class="row">
        <div class="col-md-12">
          <figure class="image pull-none"><span class="img " ><img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22400%22%20height%3D%22300%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20400%20300%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1801a0d7952%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A20pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1801a0d7952%22%3E%3Crect%20width%3D%22400%22%20height%3D%22300%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22148.83333206176758%22%20y%3D%22159%22%3E400x300%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" class="" alt="iot-5g-2.jpg" width="540"></span><figcaption><div class="caption"><p></p></div><div class="credit">
                                              Image caption 
                                          </div></figcaption></figure>
        </div>
      </div>
      {{ $blog_post['body'] }}
      <hr>
      Rating: 
      <select name="" class="star">
        <option value="">--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4" selected>4</option>
        <option value="5">5</option>
      </select> 
      <hr>

      <!-- Admin links --> 
      <br clear="all"> 
      <div>
        @if ($request->user() != null)
          @if ($request->user()->id == $blog_post['user_id'])
          <p>
            <a class="btn btn-primary" href="/blog/edit/{{ $blog_post['id'] }}" role="button">Edit Post &raquo;</a>
            <a class="btn btn-danger" href="/blog/delete/{{ $blog_post['id'] }}" role="button"
            onclick="return confirm('Are you sure you want to delete?');">Delete Post &raquo;</a>
          </p>      
          @endif
        @endif
      </div>
    </div>  
 
    <div class="col-md-4"> 
      <h2>Recent posts</h2>
      <ul class="related-posts list-unstyled">
        @foreach ($latest_posts as $post)
        <li>
          <h4>
            <a href="/blog/{{ $post['id'] }}">
              {{ $post['title'] }}            
            </a>          
          </h4>
          <small>{{ $post['created_at']->format('l jS \\of F Y h:i:s A') }}</small>
          <br>
        </li> 
        @endforeach
      </ul>
    </div>
  </div>
</div>
@endsection



@section('footer')
<script type="text/javascript">

  $('select').on('change', function() {
    
    $.get('/blog/{{ $post['id'] }}/rating/' +  this.value );
  });

</script> 
@endsection