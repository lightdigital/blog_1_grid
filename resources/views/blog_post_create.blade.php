@extends('layouts.app')

@section('content')
<div class="container"> 

  <div class="row">
    <div class="col-md-8">

    <h2>Create Blog Post</h2>

    <form method="POST" action="/blog/create/">
      @csrf <!-- {{ csrf_field() }} -->
          <div class="form-group">
            <label for="Title">Title</label>
            <input name="title" type="text" class="form-control" id="Title" value="" required>
          </div>
          <div class="form-group">
            <label for="Body">Body</label>
            <textarea name="body" class="form-control" id="Body" required=""></textarea>
          </div>     
          <div>
            <button class="btn btn-primary" type="submit">Create Post &raquo;</button>
          </div>        
    </form>
 
    </div>  
  
  </div>
</div>
@endsection