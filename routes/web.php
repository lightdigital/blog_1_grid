<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [App\Http\Controllers\BlogController::class, 'index'])->name('home');
  
Route::get('blog/', [App\Http\Controllers\BlogController::class, 'blog_listing'])->name('blog_listing'); 

Route::get('blog/create/', [App\Http\Controllers\BlogController::class, 'blog_post_create'])->name('blog_post_create'); 
Route::post('blog/create/', [App\Http\Controllers\BlogController::class, 'blog_post_create'])->name('blog_post_create'); 
 
Route::get('blog/delete/{id}', [App\Http\Controllers\BlogController::class, 'blog_post_delete'])->name('blog_post_delete'); 

Route::get('blog/edit/{id}', [App\Http\Controllers\BlogController::class, 'blog_post_edit'])->name('blog_post_edit'); 
Route::post('blog/edit/{id}', [App\Http\Controllers\BlogController::class, 'blog_post_edit'])->name('blog_post_edit'); 

Route::get('blog/{id}', [App\Http\Controllers\BlogController::class, 'blog_post'])->name('blog_post'); 

Route::get('blog/{id}/rating/{rating}', [App\Http\Controllers\BlogController::class, 'blog_post_rating'])->name('blog_post_rating'); 

