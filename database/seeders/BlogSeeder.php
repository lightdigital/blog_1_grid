<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blogposts')->insert([
            'title' => 'Millions of IoT devices running on 3G need to make the move to 5G before the older networks disappear',
            'body' => "Most of the tech world is focused on how many smartphone owners have 5G-ready devices, but there are still millions of people and services using 3G. The end is coming for 3G service in the US with AT&T is planning to turn off its 3G service in February 2022, while Verizon 3G will end for commercial and government fleet customers at the end of 2022. T-Mobile is expected to start shutting its 3G network in the last quarter of this year and to continue the process through the end of 2022.   Source: https://www.techrepublic.com/article/millions-of-iot-devices-running-on-3g-need-to-make-the-move-to-5g-before-the-older-networks-disappear/",
            'image_path' => '',
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),  
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('blogposts')->insert([
            'title' => 'FBI, Secret Service investigating cyberattack on Florida water treatment plant',
            'body' => "Local officials said someone took over their TeamViewer system and dangerously increased the levels of lye in the town's water. Federal law enforcement is now looking into a cyberattack at a water treatment plant in Oldsmar, FL where someone was able to remotely access systems and add a dangerous amount of chemicals to the town's water supply.  Source: https://www.techrepublic.com/article/fbi-secret-service-investigating-cyberattack-on-florida-water-treatment-plant/",
            'image_path' => '',
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),  
            'updated_at' => date("Y-m-d H:i:s")  
        ]);

        DB::table('blogposts')->insert([
            'title' => 'Another huge tech company just killed the 9-5 workday for good',
            'body' => "Salesforce has become the latest company to kill off the traditional nine-to-five office routine, after announcing that its employees will be free to work remotely from now on.  In a blog post published on February 9, Brent Hyder, chief people officer of the cloud CRM giant, said it \"no longer makes sense to expect employees to work an eight-hour shift and do their jobs successfully,\" and that employees would be given more flexibility in how, when and where they worked going forward.   Source: http://techrepublic.com/article/another-huge-tech-company-just-killed-the-9-5-workday-for-good/",
            'image_path' => '',
            'user_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),  
            'updated_at' => date("Y-m-d H:i:s")  
        ]);

        

    }
}